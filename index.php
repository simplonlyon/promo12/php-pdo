<?php

use App\Entity\Person;
use App\Repository\PersonRepository;

require 'vendor/autoload.php';

$repo = new PersonRepository();

foreach($repo->findAll() as $pers) {
    echo '<p>' . $pers->getName() . '</p>';
}
$person = new Person('test', 'test', 10);
$repo->add($person);

var_dump($person);