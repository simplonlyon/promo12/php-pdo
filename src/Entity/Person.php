<?php

namespace App\Entity;

class Person {
    private $id;
    private $name;
    private $personality;
    private $age;

    public function __construct(string $name, string $personality, int $age, int $id = null) {
        $this->id = $id;
        $this->name = $name;
        $this->personality= $personality;
        $this->age = $age;
    }

    public function getName():string {
        return $this->name;
    }
    public function getAge():int {
        return $this->age;
    }
    public function getPersonality():string {
        return $this->personality;
    }

    public function getId():int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }
}